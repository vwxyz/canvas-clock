var Point, canvas, clockInit, clockRun, ctx, digitNumberPoint, digitPatternAll, drawAtOnce, getColonPattern, getDigitPattern, getTimeInArray, initDraw, initDrawAnimated, initDrawAnimatedNoRandom, initDrawNoRandom, initPoints, layoutAnimated, lineBreakRamdom, margin, maxX, n, numberPerLine, onoff, pointList, previous, sideLength, slideByOneLeftAll, slideByOneRightAll, slideByOneRightAllRandom, slideByOneX, slidedTilesColon, slidedTilesH0x, slidedTilesHx0, slidedTilesM0x, slidedTilesMx0, slidedTilesS0x, slidedTilesSx0;

canvas = document.getElementById("world");

canvas.width = innerWidth;

canvas.height = innerHeight;

ctx = canvas.getContext("2d");

n = 50 * 15;

sideLength = 22;

margin = 3;

pointList = [];

maxX = innerWidth - (sideLength + margin) - innerWidth % (sideLength + margin);

numberPerLine = maxX / (sideLength + margin);

digitPatternAll = [];

Point = function(x, y, i) {
  this.x = x || 0;
  this.y = y || 0;
  this.idx = i;
  this.color = "hsla(" + (Math.random() * 360) + ", 10%, 75%, 1)";
  this.initx = Math.random() * innerWidth;
  this.inity = Math.random() * innerHeight;
  this.tempx = 0;
  this.tempy = 0;
  this.distx = 0;
  this.disty = 0;
  this.directionx = 1;
  return this.directiony = 1;
};

initPoints = function(number) {
  var i, xx, yy;
  i = 0;
  while (i < number) {
    xx = i * (sideLength + margin);
    yy = 0;
    while (xx > maxX) {
      xx = xx - innerWidth;
      xx = xx - xx % (sideLength + margin);
      yy = yy + (sideLength + margin);
    }
    pointList.push(new Point(xx, yy, i));
    i++;
  }
  return pointList.forEach(function(p) {
    ctx.fillStyle = p.color;
    if (p.x - p.initx < 0) {
      p.directionx = -1;
    }
    if (p.y - p.inity < 0) {
      p.directiony = -1;
    }
    p.tempx = p.initx;
    p.tempy = p.inity;
    p.distx = Math.floor(Math.sqrt(Math.pow(p.x - p.initx, 2)));
    return p.disty = Math.sqrt(Math.pow(p.y - p.inity, 2));
  });
};

initDraw = function(array, callback) {
  array.forEach(function(p) {
    ctx.fillStyle = p.color;
    return ctx.fillRect(p.initx, p.inity, sideLength, sideLength);
  });
  return callback();
};

initDrawNoRandom = function(array, callback) {
  array.forEach(function(p) {
    ctx.fillStyle = p.color;
    return ctx.fillRect(p.x, p.y, sideLength, sideLength);
  });
  return callback();
};

initDrawAnimated = function(array, cb) {
  var delayloop, drawRect;
  delayloop = function(array, callback, interval) {
    var i, initLayout;
    i = array.length;
    return initLayout = setInterval(function() {
      if (!i) {
        clearInterval(initLayout);
        cb();
        return;
      }
      callback(array[array.length - i]);
      return i--;
    }, interval);
  };
  drawRect = function(p) {
    ctx.fillStyle = p.color;
    if (p.x - p.initx < 0) {
      p.directionx = -1;
    }
    if (p.y - p.inity < 0) {
      p.directiony = -1;
    }
    p.tempx = p.initx;
    p.tempy = p.inity;
    p.distx = Math.floor(Math.sqrt(Math.pow(p.x - p.initx, 2)));
    p.disty = Math.sqrt(Math.pow(p.y - p.inity, 2));
    return ctx.fillRect(p.initx, p.inity, sideLength, sideLength);
  };
  return delayloop(array, drawRect, 0);
};

initDrawAnimatedNoRandom = function(array) {
  var delayloop, drawRect;
  delayloop = function(array, callback, interval) {
    var i, initLayout;
    i = array.length;
    return initLayout = setInterval(function() {
      if (!i) {
        clearInterval(initLayout);
        return;
      }
      callback(array[array.length - i]);
      return i--;
    }, interval);
  };
  drawRect = function(p) {
    ctx.fillStyle = p.color;
    if (p.x - p.initx < 0) {
      p.directionx = -1;
    }
    if (p.y - p.inity < 0) {
      p.directiony = -1;
    }
    p.tempx = p.initx;
    p.tempy = p.inity;
    p.distx = Math.floor(Math.sqrt(Math.pow(p.x - p.initx, 2)));
    p.disty = Math.sqrt(Math.pow(p.y - p.inity, 2));
    return ctx.fillRect(p.x, p.y, sideLength, sideLength);
  };
  return delayloop(array, drawRect, 0);
};

layoutAnimated = function(callback) {
  var counterx, countery;
  counterx = countery = 0;
  ctx.clearRect(0, 0, innerWidth, innerHeight);
  pointList.forEach(function(p) {
    ctx.fillStyle = p.color;
    if (Math.pow(p.x - p.tempx, 2) > 4) {
      p.tempx += p.directionx * p.distx / 30;
    } else {
      p.tempx = p.x;
    }
    if (Math.pow(p.y - p.tempy, 2) > 4) {
      p.tempy += p.directiony * p.disty / 30;
    } else {
      p.tempy = p.y;
    }
    ctx.fillRect(p.tempx, p.tempy, sideLength, sideLength);
    if (p.tempx === p.x) {
      counterx++;
    }
    if (p.tempy === p.y) {
      return countery++;
    }
  });
  if ((counterx === n) && (countery === n)) {
    clearInterval(window.move);
    return callback();
  }
};

slideByOneX = function(point) {
  point.x += sideLength + margin;
  if (point.y > innerHeight) {
    return;
  }
  if (point.x > innerWidth) {
    point.x = 0;
    return point.y += sideLength + margin;
  }
};

slideByOneRightAll = function(array, callback) {
  array.forEach(function(p) {
    p.x += sideLength + margin;
    if (p.x > maxX) {
      p.x = 0;
      return p.y += sideLength + margin;
    }
  });
  return callback();
};

slideByOneRightAllRandom = function(array) {
  var index, step;
  index = Math.floor(Math.random() * n);
  step = function() {
    slideByOneX(array[index]);
    return array.slice(index + 1, +array.length + 1 || 9e9).forEach(function(p) {
      return slideByOneX(p);
    });
  };
  step();
  setTimeout((function() {
    return step();
  }), 400);
  setTimeout((function() {
    return step();
  }), 400 * 2);
  setTimeout((function() {
    return step();
  }), 400 * 3);
  return setTimeout((function() {
    return step();
  }), 400 * 4);
};

lineBreakRamdom = function(array) {
  var distance, index, point;
  index = Math.floor(Math.random() * n);
  point = array[index];
  if (point.y > innerHeight) {
    return;
  }
  distance = point.x;
  point.x -= distance;
  point.y += sideLength + margin;
  return array.slice(index + 1, +array.length + 1 || 9e9).forEach(function(p) {
    p.x -= distance;
    if (p.x < 0) {
      p.x += innerWidth;
    }
    return p.y += sideLength + margin;
  });
};

slideByOneLeftAll = function(array, callback) {
  array.forEach(function(p) {
    if (!(p.x === 0 && p.y === 0)) {
      p.x -= sideLength + margin;
    }
    if (p.x < 0 && p.y > 0) {
      p.x = maxX;
      p.y -= sideLength + margin;
    }
    if (p.x < (sideLength + margin) && p.y === 0) {
      return p.y = 0;
    }
  });
  return callback();
};

drawAtOnce = function(array) {
  ctx.clearRect(0, 0, innerWidth, innerHeight);
  return array.forEach(function(p) {
    ctx.fillStyle = p.color;
    return ctx.fillRect(p.x, p.y, sideLength, sideLength);
  });
};

getTimeInArray = function() {
  var Arr, date;
  Arr = [];
  date = new Date();
  Arr = new Array(Math.floor(date.getHours() / 10), date.getHours() % 10, Math.floor(date.getMinutes() / 10), date.getMinutes() % 10, Math.floor(date.getSeconds() / 10), date.getSeconds() % 10);
  return Arr;
};

digitNumberPoint = function(digit, startline, baseX, sideLength, margin) {
  var arr, baseY, stepLength;
  arr = [];
  stepLength = sideLength + margin;
  baseY = stepLength * startline;
  switch (digit) {
    case 0:
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX, baseY + stepLength, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength, 1));
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 2, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 2, 1));
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 4, 3));
      return arr;
    case 1:
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 0, 1));
      arr.push(new Array(stepLength * baseX + 1 * stepLength, baseY + stepLength * 1, 2));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 2, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 4, 1));
      return arr;
    case 2:
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength, 1));
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 2, 3));
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 4, 3));
      return arr;
    case 3:
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 2, 3));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 4, 3));
      return arr;
    case 4:
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 0, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 0, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 2, 3));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 4, 1));
      return arr;
    case 5:
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 2, 3));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 4, 3));
      return arr;
    case 6:
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 2, 3));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 4, 3));
      return arr;
    case 7:
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 2, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 4, 1));
      return arr;
    case 8:
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 2, 3));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 4, 3));
      return arr;
    case 9:
      arr.push(new Array(stepLength * baseX, baseY + stepLength * 0, 3));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 1, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 2, 3));
      arr.push(new Array(stepLength * baseX + 2 * stepLength, baseY + stepLength * 3, 1));
      arr.push(new Array(stepLength * baseX + 0 * stepLength, baseY + stepLength * 4, 3));
      return arr;
  }
};

getDigitPattern = function(startline, baseX, sideLength, margin) {
  var A, arr, digit;
  A = [];
  digit = 0;
  while (digit < 10) {
    arr = digitNumberPoint(digit, startline, baseX, sideLength, margin);
    A.push(arr);
    digit++;
  }
  return A;
};

getColonPattern = function(startline, baseX, sideLength, margin) {
  var arr, baseY, stepLength;
  arr = [];
  stepLength = sideLength + margin;
  baseY = stepLength * startline;
  arr.push(new Array(stepLength * baseX, baseY + stepLength * 0, 1));
  arr.push(new Array(stepLength * baseX, baseY + stepLength * 2, 1));
  return arr;
};

window.onload = function() {
  initPoints(n);
  return initDraw(pointList, function() {
    return window.move = setInterval(function() {
      return layoutAnimated(function() {
        console.log("layoutAnimated():completed...");
        clockInit(function() {});
        window.clockId = setInterval((function() {
          return clockRun();
        }), 1000);
        return (function() {
          var square, start;
          start = performance.now();
          return (square = function() {
            var delta;
            delta = performance.now() - start;
            if (delta > 1000 * 5) {
              clockRun();
              start = performance.now();
            }
            return window.animationId = window.requestAnimationFrame(square);
          })();
        })();
      });
    }, 1000 / 30);
  });
};

onoff = Math.round(Math.random());

if (onoff === 1) {
  console.log("on/off: 1");
} else {
  console.log("on/off: 0");
}

canvas.onclick = function() {
  console.log("clicked..");
  return cancelAnimationFrame(window.animationId);
};

slidedTilesHx0 = [];

slidedTilesH0x = [];

slidedTilesMx0 = [];

slidedTilesM0x = [];

slidedTilesSx0 = [];

slidedTilesS0x = [];

slidedTilesColon = [];

previous = [];

clockInit = function(callback) {
  var arr, drawNumber, info00000X, info0000X0, info000X00, info00X000, info0X0000, infoColon, infoX00000, job, now, nowHourOnes, nowHourTens, nowMinuteOnes, nowMinuteTens, nowSecondOnes, nowSecondTens, point, previousSecondTens, retry, _i, _j, _k, _l, _len, _len1, _len10, _len11, _len2, _len3, _len4, _len5, _len6, _len7, _len8, _len9, _m, _n, _o, _p, _q, _r, _s, _t;
  now = getTimeInArray();
  previous = now;
  nowHourTens = now[0];
  nowHourOnes = now[1];
  nowMinuteTens = now[2];
  nowMinuteOnes = now[3];
  nowSecondTens = now[4];
  nowSecondOnes = now[5];
  previousSecondTens = previous[4];
  infoX00000 = getDigitPattern(1, 1, sideLength, margin)[nowHourTens];
  info0X0000 = getDigitPattern(1, 5, sideLength, margin)[nowHourOnes];
  infoColon = getColonPattern(2, 9, sideLength, margin);
  info00X000 = getDigitPattern(1, 11, sideLength, margin)[nowMinuteTens];
  info000X00 = getDigitPattern(1, 15, sideLength, margin)[nowMinuteOnes];
  info0000X0 = getDigitPattern(1, 19, sideLength, margin)[nowSecondTens];
  info00000X = getDigitPattern(1, 23, sideLength, margin)[nowSecondOnes];
  retry = function(oldjobs, newjob) {
    var a, donejob, keepjob, point, retryjob, _i, _j, _len, _len1, _results;
    keepjob = 0;
    for (_i = 0, _len = oldjobs.length; _i < _len; _i++) {
      donejob = oldjobs[_i];
      if (donejob[0] > arr[0]) {
        while (donejob[1]) {
          slideByOneLeftAll(pointList.slice(donejob[0]), function() {
            return drawAtOnce(pointList);
          });
          donejob[1]--;
        }
      } else {
        keepjob = oldjobs.indexOf(donejob);
      }
    }
    oldjobs.splice(keepjob + 1);
    _results = [];
    for (_j = 0, _len1 = newjob.length; _j < _len1; _j++) {
      retryjob = newjob[_j];
      _results.push((function() {
        var _k, _len2, _results1;
        _results1 = [];
        for (_k = 0, _len2 = pointList.length; _k < _len2; _k++) {
          point = pointList[_k];
          if (point.x === retryjob[0] && point.y === retryjob[1]) {
            a = [];
            a.push(pointList.indexOf(point));
            a.push(retryjob[2]);
            if (a[0] > arr[0]) {
              while (a[1]) {
                slideByOneRightAll(pointList.slice(a[0]), function() {
                  return drawAtOnce(pointList);
                });
                a[1]--;
              }
            }
            a[1] = retryjob[2];
            _results1.push(oldjobs.push(a));
          } else {
            _results1.push(void 0);
          }
        }
        return _results1;
      })());
    }
    return _results;
  };
  drawNumber = function(newjob, donejob, callback) {
    var arr, job, point, _i, _len, _results;
    _results = [];
    for (_i = 0, _len = newjob.length; _i < _len; _i++) {
      job = newjob[_i];
      arr = [];
      _results.push((function() {
        var _j, _len1, _results1;
        _results1 = [];
        for (_j = 0, _len1 = pointList.length; _j < _len1; _j++) {
          point = pointList[_j];
          if (point.x === job[0] && point.y === job[1]) {
            arr.push(pointList.indexOf(point));
            arr.push(job[2]);
            while (arr[1]) {
              slideByOneRightAll(pointList.slice(arr[0]), function() {
                return drawAtOnce(pointList);
              });
              arr[1]--;
            }
            arr[1] = job[2];
            donejob.push(arr);
            _results1.push(callback());
          } else {
            _results1.push(void 0);
          }
        }
        return _results1;
      })());
    }
    return _results;
  };
  if (slidedTilesHx0.length === 0) {
    drawNumber(infoX00000, slidedTilesHx0, function() {});
  }
  if (slidedTilesH0x.length === 0) {
    for (_i = 0, _len = info0X0000.length; _i < _len; _i++) {
      job = info0X0000[_i];
      arr = [];
      for (_j = 0, _len1 = pointList.length; _j < _len1; _j++) {
        point = pointList[_j];
        if (point.x === job[0] && point.y === job[1]) {
          arr.push(pointList.indexOf(point));
          arr.push(job[2]);
          while (arr[1]) {
            slideByOneRightAll(pointList.slice(arr[0]), function() {
              return drawAtOnce(pointList);
            });
            arr[1]--;
          }
          arr[1] = job[2];
          slidedTilesH0x.push(arr);
          retry(slidedTilesHx0, infoX00000);
        }
      }
    }
  }
  if (slidedTilesColon.length === 0) {
    for (_k = 0, _len2 = infoColon.length; _k < _len2; _k++) {
      job = infoColon[_k];
      arr = [];
      for (_l = 0, _len3 = pointList.length; _l < _len3; _l++) {
        point = pointList[_l];
        if (point.x === job[0] && point.y === job[1]) {
          arr.push(pointList.indexOf(point));
          arr.push(job[2]);
          while (arr[1]) {
            slideByOneRightAll(pointList.slice(arr[0]), function() {
              return drawAtOnce(pointList);
            });
            arr[1]--;
          }
          arr[1] = job[2];
          slidedTilesColon.push(arr);
          retry(slidedTilesHx0, infoX00000);
          retry(slidedTilesH0x, info0X0000);
        }
      }
    }
  }
  if (slidedTilesMx0.length === 0) {
    for (_m = 0, _len4 = info00X000.length; _m < _len4; _m++) {
      job = info00X000[_m];
      arr = [];
      for (_n = 0, _len5 = pointList.length; _n < _len5; _n++) {
        point = pointList[_n];
        if (point.x === job[0] && point.y === job[1]) {
          arr.push(pointList.indexOf(point));
          arr.push(job[2]);
          while (arr[1]) {
            slideByOneRightAll(pointList.slice(arr[0]), function() {
              return drawAtOnce(pointList);
            });
            arr[1]--;
          }
          arr[1] = job[2];
          slidedTilesMx0.push(arr);
          retry(slidedTilesHx0, infoX00000);
          retry(slidedTilesH0x, info0X0000);
          retry(slidedTilesColon, infoColon);
        }
      }
    }
  }
  if (slidedTilesM0x.length === 0) {
    for (_o = 0, _len6 = info000X00.length; _o < _len6; _o++) {
      job = info000X00[_o];
      arr = [];
      for (_p = 0, _len7 = pointList.length; _p < _len7; _p++) {
        point = pointList[_p];
        if (point.x === job[0] && point.y === job[1]) {
          arr.push(pointList.indexOf(point));
          arr.push(job[2]);
          while (arr[1]) {
            slideByOneRightAll(pointList.slice(arr[0]), function() {
              return drawAtOnce(pointList);
            });
            arr[1]--;
          }
          arr[1] = job[2];
          slidedTilesM0x.push(arr);
          retry(slidedTilesHx0, infoX00000);
          retry(slidedTilesH0x, info0X0000);
          retry(slidedTilesColon, infoColon);
          retry(slidedTilesMx0, info00X000);
        }
      }
    }
  }
  if (slidedTilesSx0.length === 0) {
    for (_q = 0, _len8 = info0000X0.length; _q < _len8; _q++) {
      job = info0000X0[_q];
      arr = [];
      for (_r = 0, _len9 = pointList.length; _r < _len9; _r++) {
        point = pointList[_r];
        if (point.x === job[0] && point.y === job[1]) {
          arr.push(pointList.indexOf(point));
          arr.push(job[2]);
          while (arr[1]) {
            slideByOneRightAll(pointList.slice(arr[0]), function() {
              return drawAtOnce(pointList);
            });
            arr[1]--;
          }
          arr[1] = job[2];
          slidedTilesSx0.push(arr);
          retry(slidedTilesHx0, infoX00000);
          retry(slidedTilesH0x, info0X0000);
          retry(slidedTilesColon, infoColon);
          retry(slidedTilesMx0, info00X000);
          retry(slidedTilesM0x, info000X00);
        }
      }
    }
  }
  if (slidedTilesS0x.length === 0) {
    for (_s = 0, _len10 = info00000X.length; _s < _len10; _s++) {
      job = info00000X[_s];
      arr = [];
      for (_t = 0, _len11 = pointList.length; _t < _len11; _t++) {
        point = pointList[_t];
        if (point.x === job[0] && point.y === job[1]) {
          arr.push(pointList.indexOf(point));
          arr.push(job[2]);
          while (arr[1]) {
            slideByOneRightAll(pointList.slice(arr[0]), function() {
              return drawAtOnce(pointList);
            });
            arr[1]--;
          }
          arr[1] = job[2];
          slidedTilesS0x.push(arr);
          retry(slidedTilesHx0, infoX00000);
          retry(slidedTilesH0x, info0X0000);
          retry(slidedTilesColon, infoColon);
          retry(slidedTilesMx0, info00X000);
          retry(slidedTilesM0x, info000X00);
          retry(slidedTilesSx0, info0000X0);
        }
      }
    }
  }
  return callback();
};

clockRun = function() {
  var allDoneJobs, deleteNumber, info00000X, info0000X0, info000X00, info00X000, info0X0000, infoColon, infoX00000, mark, now, nowHourOnes, nowHourTens, nowMinuteOnes, nowMinuteTens, nowSecondOnes, nowSecondTens;
  now = getTimeInArray();
  console.log(now);
  nowHourTens = now[0];
  nowHourOnes = now[1];
  nowMinuteTens = now[2];
  nowMinuteOnes = now[3];
  nowSecondTens = now[4];
  nowSecondOnes = now[5];
  infoX00000 = getDigitPattern(1, 1 + 5, sideLength, margin)[nowHourTens];
  info0X0000 = getDigitPattern(1, 5 + 5, sideLength, margin)[nowHourOnes];
  infoColon = getColonPattern(2, 9 + 5, sideLength, margin);
  info00X000 = getDigitPattern(1, 11 + 5, sideLength, margin)[nowMinuteTens];
  info000X00 = getDigitPattern(1, 15 + 5, sideLength, margin)[nowMinuteOnes];
  info0000X0 = getDigitPattern(1, 19 + 5, sideLength, margin)[nowSecondTens];
  info00000X = getDigitPattern(1, 23 + 5, sideLength, margin)[nowSecondOnes];
  mark = slidedTilesS0x[0][0];
  console.log("now[5]:" + now[5] + ",mark:" + mark);
  allDoneJobs = [];
  deleteNumber = function(dones, array) {
    var donejob, _i, _len;
    for (_i = 0, _len = dones.length; _i < _len; _i++) {
      donejob = dones[_i];
      if (!(donejob[0] < mark)) {
        while (donejob[1]) {
          (function() {
            var square, start;
            start = performance.now();
            return (square = function() {
              var delta;
              delta = performance.now() - start;
              if (delta > 10) {
                slideByOneLeftAll(array.slice(donejob[0]), function() {
                  return drawAtOnce(array);
                });
                start = performance.now();
              }
              return window.animationId = window.requestAnimationFrame(square);
            })();
          })();
          donejob[1]--;
        }
      }
    }
    return dones.splice(mark);
  };
  allDoneJobs = allDoneJobs.concat(slidedTilesSx0);
  allDoneJobs = allDoneJobs.concat(slidedTilesM0x);
  allDoneJobs = allDoneJobs.concat(slidedTilesMx0);
  allDoneJobs = allDoneJobs.concat(slidedTilesH0x);
  allDoneJobs = allDoneJobs.concat(slidedTilesColon);
  allDoneJobs = allDoneJobs.concat(slidedTilesHx0);
  slidedTilesS0x = slidedTilesS0x.concat(allDoneJobs);
  deleteNumber(slidedTilesS0x, pointList);
  previous = null;
  previous = now;
  clearInterval(window.clockId);
  return previous;
};
