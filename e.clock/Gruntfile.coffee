module.exports = (grunt) ->
  # configure
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    clean:["./js/*.js","index.js","index.min.js"]
    coffee:
      compile:
        files:grunt.file.expandMapping(
          ["coffee/*.coffee"]
          "js/"
          rename:(destBase,destPath)-> destBase + destPath.replace(/\.coffee$/,".js").replace "coffee/",""
        )
      options:
        bare:true
        joint:false
    concat:
      options:
        separator: ";"
      dist:
        src: [ "./js/*.js" ]
        dest: "./index.js"
    uglify:
      options:
        banner: "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd\") %> */\n"
      dist:
        src: "./index.js"
        dest: "./index.min.js"
    watch:
      files: "./coffee/*coffee"
      tasks: ["clean","coffee","concat","uglify" ]
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks "grunt-contrib-clean"
  # resigster
  grunt.registerTask "default", [ "watch" ]
