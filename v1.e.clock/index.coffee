canvas = document.getElementById("world")
canvas.width = innerWidth #innnerWidth:viewportの横サイズ
canvas.height = innerHeight #innnerWidth:viewportの縦サイズ
ctx = canvas.getContext("2d")
n = 500 #矩形数
pointList = [] #矩形の情報集約
#最初の描画
initDraw = ->
  #配列pointListのメンバーであるPointインスタンスのプロパティをもとに
  #図形を描画
  delayloop = (array, callback, interval) ->
    i = array.length
    initLayout = setInterval(->
      unless i
        clearInterval initLayout
        window.move = setInterval draw,40
        return
      callback array[array.length - i]
      i--
    , interval)
  drawRect = (p)->
    ctx.fillStyle = p.color
    p.initx = Math.random() * innerWidth
    p.inity = Math.random() * innerHeight
    if p.x-p.initx <0 then p.directionx = -1
    if p.y-p.inity <0 then p.directiony = -1
    p.tempx = p.initx
    p.tempy = p.inity
    # console.log p.directionx
    p.distx = Math.floor Math.sqrt Math.pow(p.x-p.initx,2)
    p.disty = Math.sqrt Math.pow(p.y-p.inity,2)
    # console.log p.distx # 距離x方向
    # console.log p.disty # 距離y方向
    ctx.fillRect p.initx, p.inity, 40, 40
    # ctx.fillRect p.initx, p.inity, 40, 40
  #call delayloop()
  delayloop pointList,drawRect,0

# draw()
# 矩形の移動
draw = ->
  # console.log counter 
  counterx = countery = 0
  ctx.clearRect 0, 0, innerWidth, innerHeight #コンテキストの描画内容をクリア
  pointList.forEach (p)->
    ctx.fillStyle = p.color
    #x
    if (Math.pow(p.x-p.tempx,2)>4)
      p.tempx += p.directionx*p.distx/60
      # ctx.fillRect p.tempx, p.tempy, 40, 40
    else
      p.tempx = p.x
    #y
    if (Math.pow(p.y-p.tempy,2)>4)
      p.tempy += p.directiony*p.disty/60
      # ctx.fillRect p.tempx, p.tempy, 40, 40
    else
      p.tempy = p.y

    ctx.fillRect p.tempx, p.tempy, 40, 40
    if (p.tempx is p.x) then counterx++
    if (p.tempy is p.y)
      countery++
      # console.log countery
  if (counterx is n) and (countery is n)
    clearInterval window.move
    # console.log "moved correctly.."
#Pointオブジェクト
#関数オブジェクト
Point = (x, y, i) ->
  @x = x or 0
  @y = y or 0
  @idx = i
  @color = "hsla(" + Math.random() * 360 + ", 75%, 65%, 0.85)"
  @initx = 0
  @inity = 0
  @tempx = 0
  @tempy = 0
  @distx = 0
  @disty = 0
  @directionx = 1
  @directiony = 1
#格子の初期状態として使うPointインスタンスを
#pointListへ保存
init = ->
  i = undefined  
  i = 0
  while i < n
    xx = i*40
    yy = 0
    while xx > innerWidth
      xx = xx - innerWidth
      xx = xx - xx%40
      yy = yy + 41
    pointList.push new Point(xx, yy, i)
    xx += 100
    i++
#指定間隔で
#draw()コール
window.onload = ->
  init()
  initDraw()
###
document.body.onclick = ->
  console.log "clicked."
  ctx.clearRect 0, 0, innerWidth, innerHeight #コンテキストの描画内容をクリア
  # init()
  initDraw()