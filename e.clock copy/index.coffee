# canvasエリアのセットアップ
canvas = document.getElementById("world")
canvas.width = innerWidth #innnerWidth:viewportの横サイズ
canvas.height = innerHeight #innnerWidth:viewportの縦サイズ
ctx = canvas.getContext("2d")

# 矩形の数と
# 保存用オブジェクト
n          = 50*12 #矩形数
sideLength = 22#一辺の長さ
margin     = 3# 矩形の間の余白
pointList  = [] #矩形の情報集約

# 各行の右端の矩形のx座標
maxX = innerWidth - (sideLength+margin) - innerWidth%(sideLength+margin)
# 一行あたりの矩形の数
numberPerLine = maxX/(sideLength+margin)

# 矩形、数字、座標、移動スライド数
digitPatternAll =[]


# 矩形オブジェクトを返す
# 関数
Point = (x, y, i) ->
  @x = x or 0
  @y = y or 0
  @idx = i
  # @color = "hsla(#{Math.random() * 360}, 75%, 65%, 0.85)"
  @color = "hsla(#{Math.random() * 360}, 10%, 75%, 1)"
  @initx = Math.random() * innerWidth
  @inity = Math.random() * innerHeight
  # @initx = 0
  # @inity = 0
  @tempx = 0
  @tempy = 0
  @distx = 0
  @disty = 0
  @directionx = 1
  @directiony = 1

# 格子の初期状態として使う
# Pointインスタンスを
# pointListへ保存
initPoints = (number)->
  i = 0
  # console.log maxX
  # x,y座標指定
  # 行送りあり
  while i < number
    xx = i*(sideLength+margin)
    yy = 0
    while xx > maxX
      xx = xx - innerWidth
      xx = xx - xx%(sideLength+margin)
      yy = yy + (sideLength+margin)
    pointList.push new Point(xx, yy, i)
    # xx += 100
    i++
  pointList.forEach (p)->
    ctx.fillStyle = p.color
    if p.x-p.initx <0 then p.directionx = -1
    if p.y-p.inity <0 then p.directiony = -1
    p.tempx = p.initx
    p.tempy = p.inity
    p.distx = Math.floor Math.sqrt Math.pow(p.x-p.initx,2)
    p.disty = Math.sqrt Math.pow(p.y-p.inity,2)

# 矩形描画
# 一挙に。一度だけ
initDraw = (array,callback)->
  array.forEach (p)->
    ctx.fillStyle = p.color
    ctx.fillRect p.initx,p.inity,sideLength,sideLength
  callback()

# 矩形描画
# 一挙に。一度だけ
initDrawNoRandom = (array,callback)->
  array.forEach (p)->
    ctx.fillStyle = p.color
    ctx.fillRect p.x,p.y,sideLength,sideLength
  callback()
# 最初の描画
# 矩形を１つずつ描画
initDrawAnimated = (array,cb)->
  #配列pointListのメンバーである
  #Pointインスタンスのプロパティをもとに
  #図形を描画
  delayloop = (array, callback, interval) ->
    i = array.length
    initLayout = setInterval(->
      unless i
        clearInterval initLayout
        cb()
        return
      callback array[array.length - i]
      i--
    , interval)
  drawRect = (p)->
    ctx.fillStyle = p.color
    # p.initx = Math.random() * innerWidth
    # p.inity = Math.random() * innerHeight
    if p.x-p.initx <0 then p.directionx = -1
    if p.y-p.inity <0 then p.directiony = -1
    p.tempx = p.initx
    p.tempy = p.inity
    # console.log p.directionx
    p.distx = Math.floor Math.sqrt Math.pow(p.x-p.initx,2)
    p.disty = Math.sqrt Math.pow(p.y-p.inity,2)
    # console.log p.distx # 距離x方向
    # console.log p.disty # 距離y方向
    ctx.fillRect p.initx, p.inity, sideLength, sideLength
    # ctx.fillRect p.initx, p.inity, 40, 40
  #call delayloop()
  delayloop array,drawRect,0

# 最初の描画
# 最終位置にいきなり描画
# ひとつずつ描画
initDrawAnimatedNoRandom = (array)->
  #配列pointListのメンバーであるPointインスタンスのプロパティをもとに
  #図形を描画
  delayloop = (array, callback, interval) ->
    i = array.length
    initLayout = setInterval(->
      unless i
        clearInterval initLayout
        # window.move = setInterval draw,40
        return
      callback array[array.length - i]
      i--
    , interval)
  drawRect = (p)->
    ctx.fillStyle = p.color
    # p.initx = Math.random() * innerWidth
    # p.inity = Math.random() * innerHeight
    if p.x-p.initx <0 then p.directionx = -1
    if p.y-p.inity <0 then p.directiony = -1
    p.tempx = p.initx
    p.tempy = p.inity
    # console.log p.directionx
    p.distx = Math.floor Math.sqrt Math.pow(p.x-p.initx,2)
    p.disty = Math.sqrt Math.pow(p.y-p.inity,2)
    # console.log p.distx # 距離x方向
    # console.log p.disty # 距離y方向
    ctx.fillRect p.x, p.y, sideLength, sideLength
    # ctx.fillRect p.initx, p.inity, 40, 40
  #call delayloop()
  delayloop array,drawRect,0
#----------------------------------------------------------------------#
# 整列
# 矩形の移動
# 本来の位置へ移動
layoutAnimated = (callback)->
  # console.log counter 
  counterx = countery = 0
  ctx.clearRect 0, 0, innerWidth, innerHeight #コンテキストの描画内容をクリア
  pointList.forEach (p)->
    ctx.fillStyle = p.color
    #x
    if (Math.pow(p.x-p.tempx,2)>4)
      p.tempx += p.directionx*p.distx/30
      # ctx.fillRect p.tempx, p.tempy, 40, 40
    else
      p.tempx = p.x
    #y
    if (Math.pow(p.y-p.tempy,2)>4)
      p.tempy += p.directiony*p.disty/30
      # ctx.fillRect p.tempx, p.tempy, 40, 40
    else
      p.tempy = p.y

    ctx.fillRect p.tempx, p.tempy,sideLength, sideLength
    if (p.tempx is p.x) then counterx++
    if (p.tempy is p.y)
      countery++
      # console.log countery
  if (counterx is n) and (countery is n)
    clearInterval window.move
    # console.log "layoutAnimated():tiled..."
    callback()
#----------------------------------------------------------------------#
# 矩形の移動
# x軸方向に１つ右となりへ
# 行送りつき    
slideByOneX = (point)->
  point.x += (sideLength+margin)
  if point.y > innerHeight then return
  if point.x > innerWidth
    # p.x -= innerWidth
    point.x = 0
    point.y += (sideLength+margin)

# 配列の中の全ての矩形の
# x座標を１つ横にずらす
# 行送りつき
slideByOneRightAll = (array,callback)->
  array.forEach (p)->
    p.x += (sideLength+margin)
    if p.x > maxX
      p.x = 0
      p.y += (sideLength+margin)
  callback()

slideByOneRightAllRandom = (array)->
  # 右に移動させる基準になる矩形をランダムに選ぶ
  index = Math.floor Math.random() * n
  step = ->
    slideByOneX array[index]
    array[index+1..array.length].forEach (p)->
      slideByOneX p
  # 5個分右にスライドさせる
  step()
  setTimeout (-> step()),400
  setTimeout (-> step()),400*2
  setTimeout (-> step()),400*3
  setTimeout (-> step()),400*4

# 矩形を改行させる
lineBreakRamdom = (array)->
  index    = Math.floor Math.random() * n
  point    = array[index]
  if point.y > innerHeight then return
  distance = point.x
  point.x -= distance
  point.y += (sideLength+margin)
  array[index+1..array.length].forEach (p)->
    p.x -= distance
    if p.x<0 then p.x += innerWidth 
    p.y += (sideLength+margin)
#----------------------------------------------------------------------#
# 逆向き
# 配列の中の全ての矩形の移動
# x座標を１つ左にずらず
# 逆向きの行送りつき
slideByOneLeftAll = (array,callback)->
  array.forEach (p)->
    # 左にスライド
    unless p.x is 0 and p.y is 0
      p.x -= (sideLength+margin)
    # 画面左からはみ出た時
    if p.x < 0 and p.y > 0
      # p.x  = innerWidth-(sideLength+margin) 
      p.x  = maxX 
      p.y -=  (sideLength+margin)
    if p.x < (sideLength+margin) and p.y is 0
      # console.log innerWidth%sideLength
      # p.x = 0
      p.y = 0
    # if p.x > innerWidth
    #   # p.x -= innerWidth
    #   p.x = 0
    #   p.y += (sideLength+margin)
  callback()
#----------------------------------------------------------------------#
# 配列の中にある矩形について
# 現在の矩形の情報を
# 描画する
drawAtOnce = (array)->
  ctx.clearRect 0, 0, innerWidth, innerHeight
  array.forEach (p)->
    ctx.fillStyle = p.color
    ctx.fillRect p.x, p.y, sideLength, sideLength
  # if array[0].y > innerHeight
  #   clearInterval window.slide
  #   console.log "stop slide..."
  # i = 0
  # while i < number
  #   xx = i*40
  #   yy = 0
  #   while xx > innerWidth
  #     xx = xx - innerWidth
  #     xx = xx - xx%40
  #     yy = yy + 41
  #   pointList.push new Point(xx, yy, i)
  #   xx += 100
  #   i++

#---------------------------------------------------------------
# 現在時刻を配列形式で保存
# ex.[1, 7, 1, 8, 2, 4] 
getTimeInArray =()->
  Arr = []
  date = new Date()
  Arr = new Array(
    Math.floor(date.getHours() / 10)
    date.getHours() % 10
    Math.floor(date.getMinutes() / 10)
    date.getMinutes() % 10, Math.floor(date.getSeconds() / 10)
    date.getSeconds() % 10
  )
  return Arr
#---------------------------------------------------------------
# 数字描画に必要な
# タイルの座標
# 必要なスライド
# を配列として保存
digitNumberPoint = (digit,startline,baseX,sideLength,margin)->
  # 起点タイル:x座標
  # 起点タイル:y座標
  # 移動量
  # をメンバーの配列をarrへpush
  arr =[]
  stepLength = (sideLength+margin)
  baseY = stepLength*startline
  switch digit
    when 0
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*0,3)
      # 行２：基準タイル、１マス移動。２つ隣のタイルを１マス
      arr.push new Array(stepLength*baseX,baseY+stepLength,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength,1)
      # 行３：基準タイル、１マス移動。２つ隣のタイルを１マス
      arr.push new Array(stepLength*baseX,baseY+stepLength*2,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*2,1)
      # 行４：基準タイル、１マス移動。２つ隣のタイルを１マス
      arr.push new Array(stepLength*baseX,baseY+stepLength*3,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*4,3)
      return arr
    when 1
      # 行１：１つ隣のタイル、２マス移動
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*0,1)
      # 行２：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+1*stepLength,baseY+stepLength*1,2)
      # 行３：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*2,1)
      # 行４：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*4,1)
      return arr
    when 2
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*0,3)
      # 行２：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength,1)
      # 行３：基準タイル、３マス移動。
      arr.push new Array(stepLength*baseX,baseY+stepLength*2,3)
      # 行４：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX,baseY+stepLength*3,1)
      # 行５：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*4,3)
      return arr
    when 3
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*0,3)
      # 行２：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*1,1)
      # 行３：基準タイル、３マス移動。
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*2,3)
      # 行４：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*4,3)
      return arr
    when 4
      # 行１：基準タイル、１マス移動。２つ隣のタイル、1マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*0,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*0,1)
      # 行２：基準タイル、１マス移動。２つ隣のタイル、1マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*1,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*1,1)
      # 行３：基準タイル、３マス移動。
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*2,3)
      # 行４：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：２つ隣のタイル、１マス移動。
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*4,1)
      return arr
    when 5
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*0,3)
      # 行２：基準タイル、１マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*1,1)
      # 行３：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*2,3)
      # 行４：２つ隣のタイル、１マス移動
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*4,3)
      return arr
    when 6
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*0,3)
      # 行２：基準タイル、１マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*1,1)
      # 行３：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*2,3)
      # 行４：基準タイル、１マス移動。２つ隣のタイル、１マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*3,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*4,3)
      return arr
    when 7
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*0,3)
      # 行２：基準タイル、１マス移動。２つ隣のタイル、１マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*1,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*1,1)
      # 行３：２つ隣のタイル、１マス移動
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*2,1)
      # 行４：２つ隣のタイル、１マス移動
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：２つ隣のタイル、１マス移動
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*4,1)
      return arr
    when 8
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*0,3)
      # 行２：基準タイル、１マス移動。基準タイル、１マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*1,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*1,1)
      # 行３：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*2,3)
      # 行４：基準タイル、１マス移動。２つ隣のタイル、１マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*3,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*4,3)
      return arr
    when 9
      # 行１：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX,baseY+stepLength*0,3)
      # 行２：基準タイル、１マス移動。基準タイル、１マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*1,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*1,1)
      # 行３：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*2,3)
      # 行４：基準タイル、１マス移動。２つ隣のタイル、１マス移動
      # arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*3,1)
      arr.push new Array(stepLength*baseX+2*stepLength,baseY+stepLength*3,1)
      # 行５：基準タイル、３マス移動
      arr.push new Array(stepLength*baseX+0*stepLength,baseY+stepLength*4,3)
      return arr

#----------------------------------------------------------------------#
getDigitPattern = (startline,baseX,sideLength,margin)->
  A = []
  digit = 0
  while digit < 10
    arr = digitNumberPoint digit,startline,baseX,sideLength,margin
    A.push arr
    digit++
  return A

#----------------------------------------------------------------------#
getColonPattern = (startline,baseX,sideLength,margin)->
  arr = []
  stepLength = (sideLength+margin)
  baseY = stepLength*startline
  arr.push new Array(stepLength*baseX,baseY+stepLength*0,1)
  arr.push new Array(stepLength*baseX,baseY+stepLength*2,1)
  return arr

#----------------------------------------------------------------------#
# 処理全体
window.onload = ->
  initPoints n
  # initDrawAnimated pointList,()->
  # 最初の描画
  initDraw pointList,()->
    # window.move = setInterval draw,40
    window.move = setInterval(
      ->
        # next：整列
        layoutAnimated ->
          console.log "layoutAnimated():completed..."
          #最初の時計描画
          window.clockId = setInterval (->clock()),1000
      1000/30
    )
 #end処理全体
#----------------------------------------------------------------------#
# 0 or 1を　ランダムに生成。0/1によって呼び出す関数をスイッチ
onoff = Math.round Math.random()
if onoff is 1
  console.log "on/off: 1"
else
  console.log "on/off: 0"
#----------------------------------------------------------------------#
canvas.onclick = ->
  console.log "clicked.."
  clearInterval window.clockId
#----------------------------------------------------------------------#
# スライドさせたタイルを記録
slidedTilesHx0   = [] # 時10の位（index、ステップ数）
slidedTilesH0x   = [] # 時 1の位（index、ステップ数）
slidedTilesMx0   = [] # 分10の位（index、ステップ数）
slidedTilesM0x   = [] # 分 1の位（index、ステップ数）
slidedTilesSx0   = [] # 秒10の位（index、ステップ数）
slidedTilesS0x   = [] # 秒 1の位（index、ステップ数）
slidedTilesColon = []
# 前回のターンの時刻を保存
previous = []
#----------------------------------------------------------------------#
clock = ()->
  now = getTimeInArray() # 現在時刻を配列で
  # console.log "previous:[#{previous}]"
  # console.log "now:[#{now}]" # ex. [1,5,0,0,1,0]
  nowHourTens    = now[0]  # 現在時刻、時の10の位
  nowHourOnes    = now[1]
  nowMinuteTens  = now[2]  # 現在時刻、分の10の位
  nowMinuteOnes = now[3]  # 現在時刻、分の10の位
  nowSecondTens  = now[4]  # 現在時刻、秒の10の位
  nowSecondOnes  = now[5]  # 現在時刻、秒の1の位
  #=======================
  # タイマーの直前の回との比較
  previousSecondTens = previous[4]  # 1つ前の時刻
  #=======================
  # 動作指示(＝動かすタイルのxy座標、スライド数)が5組
  infoX00000 = getDigitPattern( 1, 1,sideLength,margin)[nowHourTens]  # 時10の位：0,1,2
  info0X0000 = getDigitPattern( 1, 5,sideLength,margin)[nowHourOnes]  # 時 1の位：0,1,2,3,4,5,6,7,8,9
  infoColon  = getColonPattern( 2, 9,sideLength,margin)
  info00X000 = getDigitPattern( 1,11,sideLength,margin)[nowMinuteTens]  # 分10の位：0,1,2,3,4,5
  info000X00 = getDigitPattern( 1,15,sideLength,margin)[nowMinuteOnes] # 分 1の位：0,1,2,3,4,5,6,7,8,9
  info0000X0 = getDigitPattern( 7,11,sideLength,margin)[nowSecondTens] # 秒10の位：0,1,2,3,4,5,
  info00000X = getDigitPattern( 7,15,sideLength,margin)[nowSecondOnes] # 秒 1の位：0,1,2,3,4,5,6,7,8,9
  #---------------------------------------------------------#
  retry = (oldjobs,newjob)->
    keepjob = 0
    for donejob in oldjobs
      if (donejob[0] > arr[0])
        while donejob[1]
          slideByOneLeftAll pointList[donejob[0]..], -> drawAtOnce pointList
          donejob[1]--
      else
        keepjob = oldjobs.indexOf donejob
    # 再描画
    oldjobs.splice(keepjob+1) #消したjobはまとめて消す
    for retryjob in newjob
      for point in pointList
        if point.x is retryjob[0] and point.y is retryjob[1] # タイルのx,座標で判定
          a =[]
          a.push pointList.indexOf point # タイルのindex保存
          a.push retryjob[2]                  # タイルをスライドさせる回数 　
          if a[0] > arr[0]
            while a[1]
              slideByOneRightAll pointList[a[0]..], -> drawAtOnce pointList
              a[1]--
          a[1] = retryjob[2]
          oldjobs.push a # jobリストへjobを追加
  #---------------------------------------------------------#
  drawNumber = (newjob,donejob,callback)->
    for job in newjob
      arr =[]
      for point in pointList
        if point.x is job[0] and point.y is job[1] # タイルのx,座標で判定
          arr.push pointList.indexOf point # タイルのindex保存
          arr.push job[2]                # タイルをスライドさせる回数 　
          # タイルのスライド
          # ターゲット〜末尾
          # arr[1]回スライド
          # console.log "job::matched:[#{arr[0]}]"
          while arr[1]
            slideByOneRightAll pointList[arr[0]..], -> drawAtOnce pointList
            arr[1]--
          arr[1] = job[2]
          donejob.push arr
          callback()
  #---------------------------------------------------------#
  # hh時mm分：描画
  # hh時描画：10の位
  if slidedTilesHx0.length is 0
    drawNumber infoX00000,slidedTilesHx0, ->
  # hh時描画：1の位
  if slidedTilesH0x.length is 0
    # drawNumber info0X0000,slidedTilesH0x,->
    #   retry slidedTilesHx0,infoX00000
    for job in info0X0000
      arr =[]
      for point in pointList
        if point.x is job[0] and point.y is job[1] # タイルのx,座標で判定
          arr.push pointList.indexOf point # タイルのindex保存
          arr.push job[2]                # タイルをスライドさせる回数 　
          # タイルのスライド
          # ターゲット〜末尾
          # arr[1]回スライド
          # console.log "job::matched:[#{arr[0]}]"
          while arr[1]
            slideByOneRightAll pointList[arr[0]..], -> drawAtOnce pointList
            arr[1]--
          arr[1] = job[2]
          slidedTilesH0x.push arr
          # ずれてる所全部消し：retry()
          retry slidedTilesHx0,infoX00000
  #---------------------------------------------------------#
  # コロン描画：
  if slidedTilesColon.length is 0
    for job in infoColon
      arr =[]
      for point in pointList
        if point.x is job[0] and point.y is job[1] # タイルのx,座標で判定
          arr.push pointList.indexOf point # タイルのindex保存
          arr.push job[2]                # タイルをスライドさせる回数 　
          # タイルのスライド
          # ターゲット〜末尾
          # arr[1]回スライド
          # console.log "job::matched:[#{arr[0]}]"
          while arr[1]
            slideByOneRightAll pointList[arr[0]..], -> drawAtOnce pointList
            arr[1]--
          arr[1] = job[2]
          slidedTilesColon.push arr
          # ずれてる所全部消し：retry()
          retry slidedTilesHx0,infoX00000
          retry slidedTilesH0x,info0X0000
  #---------------------------------------------------------#
  # mm分描画：10の位
  if slidedTilesMx0.length is 0

    # drawNumber info00X000,slidedTilesMx0,->
    #   retry slidedTilesHx0,infoX00000
    #   retry slidedTilesH0x,info0X0000

    for job in info00X000
      arr =[]
      for point in pointList
        if point.x is job[0] and point.y is job[1] # タイルのx,座標で判定
          arr.push pointList.indexOf point # タイルのindex保存
          arr.push job[2]                # タイルをスライドさせる回数 　
          # タイルのスライド
          # ターゲット〜末尾
          # arr[1]回スライド
          # console.log "job::matched:[#{arr[0]}]"
          while arr[1]
            slideByOneRightAll pointList[arr[0]..], -> drawAtOnce pointList
            arr[1]--
          arr[1] = job[2]
          slidedTilesMx0.push arr
          # ずれてる所全部消し：retry()
          retry slidedTilesHx0,infoX00000
          retry slidedTilesH0x,info0X0000
          retry slidedTilesColon,infoColon

  if slidedTilesM0x.length is 0
    # drawNumber info000X00,slidedTilesM0x,->
    #       retry slidedTilesHx0,infoX00000
    #       retry slidedTilesH0x,info0X0000
    #       retry slidedTilesMx0,info00X000

    for job in info000X00
      arr =[]
      for point in pointList
        if point.x is job[0] and point.y is job[1] # タイルのx,座標で判定
          arr.push pointList.indexOf point # タイルのindex保存
          arr.push job[2]                # タイルをスライドさせる回数 　
          # タイルのスライド
          # ターゲット〜末尾
          # arr[1]回スライド
          # console.log "job::matched:[#{arr[0]}]"
          while arr[1]
            slideByOneRightAll pointList[arr[0]..], -> drawAtOnce pointList
            arr[1]--
          arr[1] = job[2]
          slidedTilesM0x.push arr
          # ずれてる所全部消し：retry()
          retry slidedTilesHx0,infoX00000
          retry slidedTilesH0x,info0X0000
          retry slidedTilesColon,infoColon
          retry slidedTilesMx0,info00X000
  #---------------------------------------------------------# 
  # 描画：秒10の位
  #=======================
  # 初回描画：秒10の位
  if slidedTilesSx0.length is 0 then drawNumber info0000X0,slidedTilesSx0,->
  #---------------------------------------------------------# 
  # 初回描画：秒1の位、10の位は描画済
  if slidedTilesS0x.length is 0
    console.log "10の位だけ描画されてる"
    for index,job of info00000X
      for point in pointList
        if point.x is job[0] and point.y is job[1] # タイルの[x,y]座標で判定
          arr = []
          arr.push pointList.indexOf point # タイルのindex保存
          arr.push job[2]                # タイルをスライドさせる回数
          while arr[1]
            slideByOneRightAll pointList[arr[0]..], -> drawAtOnce pointList
            arr[1]--
          arr[1] = job[2]
          slidedTilesS0x.push arr
          # ずれてる所全部消し
          retry slidedTilesSx0,info0000X0
  #---------------------------------------------------------# 
  # 更新：秒1の位
  if slidedTilesS0x.length isnt 0
    # 一気に消す：秒1の位
    for donejob in slidedTilesS0x
      while donejob[1]
        slideByOneLeftAll pointList[donejob[0]..], -> drawAtOnce pointList
        donejob[1]--
     # ずれてる所全部消し
    # ------------------------
    if nowSecondOnes is 0
      for donejob in slidedTilesSx0
        while donejob[1]
          slideByOneLeftAll pointList[donejob[0]..], -> drawAtOnce pointList
          donejob[1]--
      slidedTilesSx0 = []
    # ------------------------
    else
      for donejob in slidedTilesSx0
        keepjob = 0
        milestone = slidedTilesS0x[0][0]
        if (donejob[0] > milestone )
          while donejob[1]
            slideByOneLeftAll pointList[donejob[0]..], -> drawAtOnce pointList
            donejob[1]--
        else
          keepjob = slidedTilesSx0.indexOf donejob
      slidedTilesSx0.splice(keepjob+1,) #消したjobはまとめて消す
    # ------------------------
    # 再描画
    for retryjob in info0000X0
      for point in pointList
        if point.x is retryjob[0] and point.y is retryjob[1] # タイルのx,座標で判定
          a =[]
          a.push pointList.indexOf point # タイルのindex保存
          a.push retryjob[2]                  # タイルをスライドさせる回数 　
          # if a[0] > arr[0]
          while a[1]
            slideByOneRightAll pointList[a[0]..], -> drawAtOnce pointList
            a[1]--
          a[1] = retryjob[2]
          slidedTilesSx0.push a # jobリストへjobを追加
    for index,job of info00000X
      for point in pointList
        if point.x is job[0] and point.y is job[1] # タイルの[x,y]座標で判定
          arr = []
          arr.push pointList.indexOf point # タイルのindex保存
          arr.push job[2]                # タイルをスライドさせる回数
          while arr[1]
            slideByOneRightAll pointList[arr[0]..], -> drawAtOnce pointList
            arr[1]--
          arr[1] = job[2]
          slidedTilesS0x.push arr
          # ずれてる所全部消し
          retry slidedTilesSx0,info0000X0
  #---------------------------------------------------------#
  #最後に：時間、doneしたjob情報：保存
  previous = now
  console.log "最後：previous:       #{previous}"
  console.log "最後：slidedTilesSx0: #{slidedTilesSx0}"
  console.log "最後：slidedTilesS0x: #{slidedTilesS0x}"
