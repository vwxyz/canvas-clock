module.exports = (grunt) ->
  # configure
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    uglify:
      options:
        banner: "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd\") %> */\n"
      dist:
        src: "./index.js"
        dest: "./dist/index.min.js"
    concat:
      options:
        separator: ";"
      dist:
        src: [ "./index.js", "./clock.js" ]
        dest: "./dist/all.js"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-concat"
  # resigster
  grunt.registerTask "default", [ "uglify","concat" ]
  # grunt.registerTask "default", [ "uglify" ]
